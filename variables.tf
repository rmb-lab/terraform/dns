variable "cloudflare_email" {
    type = "string"
}

variable "cloudflare_token" {
  type = "string"
}

variable "root_domain" {
    type = "string"
    default = "rmb938.me"
}