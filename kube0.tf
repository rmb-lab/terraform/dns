resource "cloudflare_record" "kube0-master" {
  domain = "${var.root_domain}"
  name   = "kube0-master.vmw"
  type   = "A"
  value  = "192.168.23.100"
}

locals {
  worker_ips = [
    "192.168.23.101",
    "192.168.23.102",
    "192.168.23.103",
  ]
}

resource "cloudflare_record" "kube0-worker" {
  count  = "${length(local.worker_ips)}"
  domain = "${var.root_domain}"
  name   = "kube0-worker${count.index}.vmw"
  type   = "A"
  value  = "${element(local.worker_ips, count.index)}"
}

resource "cloudflare_record" "kube0-ing" {
  count  = "${length(local.worker_ips)}"
  domain = "${var.root_domain}"
  name   = "kube0-ing.vmw"
  type   = "A"
  value  = "${element(local.worker_ips, count.index)}"
}

resource "cloudflare_record" "wildcard-kube0-ing" {
  count  = "${length(local.worker_ips)}"
  domain = "${var.root_domain}"
  name   = "*.kube0-ing.vmw"
  type   = "A"
  value  = "${element(local.worker_ips, count.index)}"
}
