terraform {
  backend "gcs" {
    bucket = "rmb-lab-terraform"
    prefix = "dns"
  }
}

provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}
